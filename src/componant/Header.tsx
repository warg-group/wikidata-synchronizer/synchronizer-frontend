import React from 'react';
import { useNavigate } from 'react-router-dom';

const Header = (): JSX.Element => {
    const navigate = useNavigate();

    return (
        <div className="flex-row">
            <button onClick={() => navigate('/')}>Home</button>
            <button onClick={() => navigate('/page')}>Page 1</button>
        </div>
    );
};

export { Header };
