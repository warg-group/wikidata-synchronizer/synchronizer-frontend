import React from 'react';
import { Route, Routes, BrowserRouter as Router } from 'react-router-dom';
import { Home } from './componant/page/Home';
import { Page } from './componant/page/Page';
import { Header } from './componant/Header';

const Application = (): JSX.Element => {
    return (
        <Router>
            <Header />

            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/page" element={<Page />} />
            </Routes>
        </Router>
    );
};

export default Application;
